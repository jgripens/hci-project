
var data = [
  {"title": "Survey1",
    "state": "Alabama",
    "type": "median household income",
    "topline": "$46,472",
    "survey": "American Community Survey",
    "image": "charts/alabamaincome.png"
  },
  {"title": "Survey2",
    "state": "Wisconsin",
    "type": "median household income",
    "topline": "$56,759",
    "survey": "American Community Survey",
    "image": "charts/wisconsinincome.png"
  },
  {"title": "Survey3",
    "state": "Illinois",
    "type": "median household income",
    "topline": "$61,229",
    "survey": "American Community Survey",
    "image": "charts/illinoisincome.png"
  },
  {"title": "Survey4",
    "state": "Nebraska",
    "type": "median household income",
    "topline": "$56,675",
    "survey": "American Community Survey",
    "image": "charts/nebraskaincome.png"
  },
  {"title": "Survey5",
    "state": "New Jersey",
    "type": "median household income",
    "topline": "$76,475",
    "survey": "American Community Survey",
    "image": "charts/newjerseyincome.png"
  },
  {"title": "Survey6",
    "state": "Ohio",
    "type": "median household income",
    "topline": "$52,407",
    "survey": "American Community Survey",
    "image": "charts/ohioincome.png"
  },
  {"title": "Survey7",
    "state": "Tennessee",
    "type": "median household income",
    "topline": "$48,708",
    "survey": "American Community Survey",
    "image": "charts/tennesseeincome.png"
  },
  {"title": "Survey8",
    "state": "California",
    "type": "median household income",
    "topline": "$67,169",
    "survey": "American Community Survey",
    "image": "charts/californiaincome.png"
  },
  {"title": "Survey9",
    "state": "Montana",
    "type": "median household income",
    "topline": "$50,801",
    "survey": "American Community Survey",
    "image": "charts/montanaincome.png"
  },
  {"title": "Survey10",
    "state": "Alabama",
    "type": "population",
    "topline": "4,887,871",
    "survey": "U.S. Census 2018 Estimates",
    "image": "charts/alabamapop.png"
  },
  {"title": "Survey11",
    "state": "North Dakota",
    "type": "population",
    "topline": "760,077",
    "survey": "U.S. Census 2018 Estimates",
    "image": "charts/northdakotapop.png"
  },
  {"title": "Survey12",
    "state": "Texas",
    "type": "population",
    "topline": "28,701,845",
    "survey": "U.S. Census 2018 Estimates",
    "image": "charts/texaspop.png"
  },
  {"title": "Survey13",
    "state": "Vermont",
    "type": "population",
    "topline": "626,299",
    "survey": "U.S. Census 2018 Estimates",
    "image": "charts/vermontpop.png"
  },
  {"title": "Survey14",
    "state": "Michigan",
    "type": "population",
    "topline": "9,995,915",
    "survey": "U.S. Census 2018 Estimates",
    "image": "charts/michiganpop.png"
  },
  {"title": "Survey15",
    "state": "Colorado",
    "type": "population",
    "topline": "5,695,564",
    "survey": "U.S. Census 2018 Estimates",
    "image": "charts/coloradopop.png"
  },
  {"title": "Survey16",
    "state": "Georgia",
    "type": "population",
    "topline": "10,519,475",
    "survey": "U.S. Census 2018 Estimates",
    "image": "charts/georgiapop.png"
  },
  {"title": "Survey17",
    "state": "Indiana",
    "type": "population",
    "topline": "6,691,878",
    "survey": "U.S. Census 2018 Estimates",
    "image": "charts/indianapop.png"
  },
  {"title": "Survey18",
    "state": "Oregon",
    "type": "population",
    "topline": "4,190,713",
    "survey": "U.S. Census 2018 Estimates",
    "image": "charts/oregonpop.png"
  },
  {"title": "Survey19",
    "state": "New Hampshire",
    "type": "median age",
    "topline": 42.7,
    "survey": "American Community Survey",
    "image": "charts/newhampshiremedianage.png"
  },
  {"title": "Survey20",
    "state": "Florida",
    "type": "median age",
    "topline": 41.8,
    "survey": "American Community Survey",
    "image": "charts/floridamedianage.png"
  },
  {"title": "Survey21",
    "state": "Washington",
    "type": "median age",
    "topline": 37.6,
    "survey": "American Community Survey",
    "image": "charts/washingtonmedianage.png"
  },
  {"title": "Survey22",
    "state": "Iowa",
    "type": "median age",
    "topline": 38.1,
    "survey": "American Community Survey",
    "image": "charts/iowamedianage.png"
  },
  {"title": "Survey23",
    "state": "Arizona",
    "type": "median age",
    "topline": 37.2,
    "survey": "American Community Survey",
    "image": "charts/arizonamedianage.png"
  },
  {"title": "Survey24",
    "state": "Kentucky",
    "type": "median age",
    "topline": 38.6,
    "survey": "American Community Survey",
    "image": "charts/kentuckymedianage.png"
  },
  {"title": "Survey25",
    "state": "South Dakota",
    "type": "number of companies",
    "topline": "81,314",
    "survey": "Survey of Business Owners",
    "image": "charts/southdakotacompanies.png"
  },
  {"title": "Survey26",
    "state": "Virginia",
    "type": "number of companies",
    "topline": "653,193",
    "survey": "Survey of Business Owners",
    "image": "charts/virginiacompanies.png"
  },
  {"title": "Survey27",
    "state": "Utah",
    "type": "number of companies",
    "topline": "251,419",
    "survey": "Survey of Business Owners",
    "image": "charts/utahcompanies.png"
  },
  {"title": "Survey28",
    "state": "Louisiana",
    "type": "number of companies",
    "topline": "414,291",
    "survey": "Survey of Business Owners",
    "image": "charts/louisianacompanies.png"
  },
  {"title": "Survey29",
    "state": "Minnesota",
    "type": "number of companies",
    "topline": "4,899,494",
    "survey": "Survey of Business Owners",
    "image": "charts/minnesotacompanies.png"
  },
  {"title": "Survey30",
    "state": "South Carolina",
    "type": "percent high school graduate",
    "topline": "86.5%",
    "survey": "American Community Survey",
    "image": "charts/southcarolinaeducation.png"
  },
  {"title": "Survey31",
    "state": "Kansas",
    "type": "percent high school graduate",
    "topline": "90.5%",
    "survey": "American Community Survey",
    "image": "charts/southcarolinaeducation.png"
  },
  {"title": "Survey32",
    "state": "Idaho",
    "type": "percent high school graduate",
    "topline": "90.2%",
    "survey": "American Community Survey",
    "image": "charts/idahoeducation.png"
  },
  {"title": "Survey33",
    "state": "Maine",
    "type": "percent high school graduate",
    "topline": "92.1%",
    "survey": "American Community Survey",
    "image": "charts/maineeducation.png"
  },
  {"title": "Survey34",
    "state": "Wisconsin",
    "type": "percent high school graduate",
    "topline": "91.7%",
    "survey": "American Community Survey",
    "image": "charts/wisconsineducation.png"
  },
  {"title": "Survey35",
    "state": "Hawaii",
    "type": "percent high school graduate",
    "topline": "91.6%",
    "survey": "American Community Survey",
    "image": "charts/hawaiieducation.png"
  },
  {"title": "Survey36",
    "state": "Nevada",
    "type": "percent high school graduate",
    "topline": "85.8%",
    "survey": "American Community Survey",
    "image": "charts/nevadaeducation.png"
  },
  {"title": "Survey37",
    "state": "Pennsylvania",
    "type": "total housing units",
    "topline": "5,653,599",
    "survey": "American Housing Survey",
    "image": "charts/pennsylvaniahousing.png"
  },
  {"title": "Survey38",
    "state": "Maryland",
    "type": "total housing units",
    "topline": "2,427,014",
    "survey": "American Housing Survey",
    "image": "charts/marylandhousing.png"
  },
  {"title": "Survey39",
    "state": "Oklahoma",
    "type": "total housing units",
    "topline": "1,712,841",
    "survey": "American Housing Survey",
    "image": "charts/oklahomahousing.png"
  },
  {"title": "Survey40",
    "state": "Wyoming",
    "type": "total housing units",
    "topline": "273,088",
    "survey": "American Housing Survey",
    "image": "charts/wyominghousing.png"
  },
  {"title": "Survey41",
    "state": "Alaska",
    "type": "percent below poverty level",
    "topline": "10.2%",
    "survey": "American Community Survey",
    "image": "charts/alaskapoverty.png"
  },
  {"title": "Survey42",
    "state": "Rhode Island",
    "type": "percent below poverty level",
    "topline": "13.4%",
    "survey": "American Community Survey",
    "image": "charts/rhodeislandpoverty.png"
  },
  {"title": "Survey43",
    "state": "New Mexico",
    "type": "percent below poverty level",
    "topline": "20.6%",
    "survey": "American Community Survey",
    "image": "charts/newmexicopoverty.png"
  },
  {"title": "Survey44",
    "state": "North Carolina",
    "type": "percent below poverty level",
    "topline": "16.1%",
    "survey": "American Community Survey",
    "image": "charts/northcarolinapoverty.png"
  },
  {"title": "Survey45",
    "state": "Arkansas",
    "type": "percent below poverty level",
    "topline": "18.1%",
    "survey": "American Community Survey",
    "image": "charts/arkansaspoverty.png"
  },
  {"title": "Survey46",
    "state": "West Virginia",
    "type": "percent below poverty level",
    "topline": "17.8%",
    "survey": "American Community Survey",
    "image": "charts/westvirginiapoverty.png"
  },
  {"title": "Survey47",
    "state": "Iowa",
    "type": "percent below poverty level",
    "topline": "12.0%",
    "survey": "American Community Survey",
    "image": "charts/iowapoverty.png"
  },
  {"title": "Survey48",
    "state": "Delaware",
    "type": "lottery proceeds",
    "topline": "$239,519",
    "survey": "U.S. Census Bureau",
    "image": "charts/delawarelottery.png"
  },
  {"title": "Survey49",
    "state": "New York",
    "type": "lottery proceeds",
    "topline": "$3,292,253",
    "survey": "U.S. Census Bureau",
    "image": "charts/newyorklottery.png"
  },
  {"title": "Survey50",
    "state": "Massachusetts",
    "type": "lottery proceeds",
    "topline": "$1,320,783",
    "survey": "U.S. Census Bureau",
    "image": "charts/massachusettslottery.png"
  },
  {"title": "Survey51",
    "state": "Connecticut",
    "type": "lottery proceeds",
    "topline": "$336,181",
    "survey": "U.S. Census Bureau",
    "image": "charts/connecticutlottery.png"
  },
  {"title": "Survey52",
    "state": "Missouri",
    "type": "lottery proceeds",
    "topline": "$273,170",
    "survey": "U.S. Census Bureau",
    "image": "charts/missourilottery.png"
  }
]

  
title = document.URL.split('?')[1];

  for(var i = 0; i < data.length; i++)
  {
      if(title == data[i]['title'])
      {
        var state = data[i]['state'];
        var type = data[i]['type'];
        var sur = data[i]['survey'];
        var image = data[i]['image'];
        var topline = data[i]['topline'];
        console.log(state);
        break;
      }
  }
  document.getElementById('survey').innerText = sur;
  document.getElementById('geo').innerText = state;
  document.getElementById('question').innerText = type;
  document.getElementById('var1').innerText = type;
  document.getElementById('var2').innerText = state;
  document.getElementById('chart').src = "../"+image;
  document.getElementById('result').innerText = topline;

console.log(data);

